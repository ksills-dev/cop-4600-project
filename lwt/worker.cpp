#include "worker.hpp"

#include "foreman.hpp"

#include <pthread.h>

namespace lwt {
namespace internal {

// Just a global for a zero
// time interval (used for
// disabling preemption
// timers).
static timespec const zero_time_interval = {0};
static timespec const zero_time_value = {0};
static itimerspec const zero_time = {zero_time_interval, zero_time_value};

Worker::Worker(Foreman *master)
    : _alive(true), _killed(false), _master(master), _loaded(false),
      _stack_size(master->_stack_size) {
  // Generate the actual worker thread.
  pthread_attr_t attrs;
  pthread_attr_init(&attrs);
  pthread_attr_setdetachstate(&attrs, PTHREAD_CREATE_JOINABLE);
  pthread_create(&_thread_handle, &attrs,
                 (void *(*)(void *)) & Worker::worker_thread_entry, this);
}

Worker::~Worker() {
  // Currently this has nothing to do!
}

auto Worker::die() -> void {
  // Disable the preemption timer immediately!
  timer_settime(_preempt_timer, 0, &zero_time, 0);

  if (_loaded) {
    // Push our job into the queue so another worker can complete it.
    pthread_mutex_lock(&_master->_jq_lock);
    _master->_job_queue.push(_current);
    pthread_mutex_unlock(&_master->_jq_lock);
  }

  // Notify the world of our sacrifice and clean resources.
  _alive = false;
  free(_job_exit_target.uc_stack.ss_sp);
  timer_delete(_preempt_timer);
  pthread_exit(0);
}

auto Worker::run_job(Job &job) -> void {
  // Grab the job and reassign it's exit target.
  _current = job;
  *job.exit_target = _job_exit_target;

  // Enable our preemption timer and jump to context.
  timer_settime(_preempt_timer, 0, &_master->_preempt_time, 0);
  setcontext(job.context);
}

auto Worker::schedule() -> void {
  // Since we have no job, just keep looping and waiting for a new job on the
  // queue or a death state. We **must** perform a timed wait here with the
  // current implementation, as otherwise it will be messy to cleanly stop a
  // worker awaiting a new job.
  while (true) {
    if (_killed) {
      die();
    }
    if (sem_timedwait(_master->_free_jobs,
                      &_master->_preempt_time.it_interval) == 0) {
      break;
    } else {
      if (not _master->_alive) {
        die();
      }
    }
  }

  // Grab our new job from the queue.
  pthread_mutex_lock(&_master->_jq_lock);
  Job next = _master->_job_queue.front();
  _master->_job_queue.pop();
  pthread_mutex_unlock(&_master->_jq_lock);

  // Run that bad boy.
  _loaded = true;
  run_job(next);
}

auto Worker::reschedule() -> void {
  if (_killed) {
    die();
  }

  // Grab the next job available in the round-robin queue.
  pthread_mutex_lock(&_master->_jq_lock);

  // Just use the current job if there aren't any other jobs waiting.
  Job next = _current;
  if (_master->_job_queue.size() == 0) {
    pthread_mutex_unlock(&_master->_jq_lock);
    return;
  }

  // Reentrant allows the context to differentiate on returning (prevents
  // infinite get/set context loop).
  *_current.reentrant = false;
  getcontext(_current.context);
  if (*_current.reentrant == false) {
    *_current.reentrant = true;
    pthread_mutex_unlock(&_master->_jq_lock);

    // Run that bad girl.
    run_job(next);
  }

  // Back up the stack when original context restored.
}

auto Worker::yield() -> void {
  // Disable preemption timer immediately!
  timer_settime(_preempt_timer, 0, &zero_time, 0);
  reschedule();
  timer_settime(_preempt_timer, 0, &_master->_preempt_time, 0);
}

auto Worker::finalize_job() -> void {
  // Disable preemption timer immediately!
  timer_settime(_preempt_timer, 0, &zero_time, 0);

  // Unload the job, clean it's resources.
  _loaded = false;
  free(_current.context->uc_stack.ss_sp);
  delete _current.context;
  delete _current.exit_target;
  delete _current.reentrant;

  // Move worker back to idle scheduling.
  schedule();
}

} // namespace internal
} // namespace lwt
