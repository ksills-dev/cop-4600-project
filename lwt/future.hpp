#pragma once

#include <atomic>
#include <functional>

#include <semaphore.h>

namespace lwt {

/** Basic Future type for use with lwt::Foreman.
 */
template <class T> class Future {
  // These asserts aren't strictly necessary, but alleviate my current conflict
  // with thinking.
  static_assert(std::is_copy_constructible<T>::value,
                "Future T must be copy constructible.");
  static_assert(std::is_move_constructible<T>::value,
                "Future T must be move constructible.");
  static_assert(std::is_copy_assignable<T>::value,
                "Future T must be copy assignable.");
  static_assert(std::is_move_assignable<T>::value,
                "Future T must be move assignable.");

public:
  /** Constructor.
   */
  Future() {
    _content = new Content();
    sem_init(&_content->notify, 0, -1);
    _content->count = 1;
    _content->waiting = 0;
    _content->ready = false;
  }

  /** Copy Constructor.
   */
  Future(Future const &original) : _content(original._content) {
    _content->count += 1; // Increment refcount.
  }

  /** Move constructor.
   */
  Future(Future &&original) : _content(original._content) {
    original._content = nullptr;
  }

  /** Copy assignment.
   */
  auto operator=(Future const &original) -> Future & {
    _content = original._content;
    _content->count += 1; // Increment refcount.
  }

  /** Move assignment.
   */
  auto operator=(Future &&original) -> Future & {
    _content = original._content;
    original._content = nullptr;
    return this;
  }

  /** Destructor.
   * Only frees resources when all references have been released and future was
   * satisfied.
   */
  ~Future() {
    if (_content == nullptr) {
      // Future content was moved, don't attempt destruction.
      return;
    }

    if (--_content->count == 0) {
      sem_destroy(&_content->notify);
      if (_content->ready) {
        _content->value.~T(); // Unions do not follow RAII, must manually delete
                              // if an object exits.
      }
      delete _content;
      _content = nullptr;
    }
  }

  /** Returns true if the future has been satisfied, false otherwise.
   */
  auto ready() const -> bool { return _content->ready; }

  /** Returns true if the future has been satisfied, false otherwise.
   */
  operator bool() const { return _content->ready; }

  /** Attempts to assign the satisfied future to the target reference.
   * Returns whether or not future was available and assignment was completed.
   */
  auto try_fetch(T &target) -> bool {
    if (not _content->ready) {
      return false;
    }
    target = _content->value;
    return true;
  }

  /** Fetch the future's value, waiting if not yet satisfied.
   */
  auto wait_fetch() -> T {
    if (not _content->ready) {
      _content->waiting++;
      sem_wait(&_content->notify);
    }
    return _content->value;
  }

  /** Implicitly convert future to value, waiting if not yet satisfied.
   */
  operator T() { return wait_fetch(); }

protected:
  friend class Foreman; // Only give access to fulfill to the Foreman. Prevents
                        // having to have an accompanied promise class.

  /** Fulfill the future with the provided value and mark as satisfied
   * (informing any waiting party).
   *
   * @note The value provided must be an rvalue (because that's what makes
   * sense).
   */
  auto fulfill(T &&value) -> void {
    new (&_content->value) T(std::move(value));
    _content->ready = true;
    while (_content->waiting-- > 0) {
      sem_post(&_content->notify);
    }
  }

private:
  struct Content {
    sem_t notify;             ///< Semaphore to notify any waiting parties.
    std::atomic_bool ready;   ///< True if the future has been satisfied.
    std::atomic<int> waiting; ///< Number of parties currently waiting on the
                              ///< future to be satisfied.
    std::atomic<int> count;   ///< Number of references to this future object.
    union {
      T value; ///< The value returned by job execution. Within a union to
               ///< prevent automagic initialization / destruction.
    };
  } * _content; ///< Pointer, because sharing is caring.
};

} // namespace lwt
