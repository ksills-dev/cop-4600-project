#pragma once

#include "job.hpp"

#include <atomic>
#include <iostream>

#include <pthread.h>
#include <semaphore.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/syscall.h>
#include <time.h>
#include <unistd.h>

namespace lwt {
class Foreman; // Forward declare of master type.

namespace internal { // Worker currently in the internal namespace.

using sigaction_t =
    struct sigaction; // Just a nice alias to avoid struct namespace.

/** Worker thread object for our Foreman thread pool.
 *
 * @see Foreman
 * @internal
 */
class Worker {
public:
  /// Default destructor.
  ~Worker();

  /** Yield function for executing processes (currently unused).
   * TODO: Might be better for a job to grab a Foreman pointer and execute a
   * yield from there. Could demultiplex via executing tid.
   */
  auto yield() -> void;

protected:
  friend lwt::Foreman; // Forward declare of master.

  /// Basic constructor
  Worker(lwt::Foreman *master);

  // Delete these nasty things.
  Worker(const Worker &original) = delete;
  Worker(Worker &&original) = delete;
  auto operator=(const Worker &original) -> Worker & = delete;
  auto operator=(Worker &&original) -> Worker & = delete;

  pthread_t _thread_handle; ///< Worker thread's pthread handle.
  pid_t _tid;               ///< Worker thread's TID.
  std::atomic_bool _alive;  ///< Set to false just before pthread_exit called.
  std::atomic_bool
      _killed; ///< If set to true, worker will die on next scheduling cycle.

private:
  Foreman *const _master;      ///< Backreference to master.
  ucontext_t _job_exit_target; ///< Contains the context to return to when job
                               ///< execution completes.

  bool _loaded; ///< Set to true if a job is loaded.
  union {
    Job _current; ///< Job currently loaded for execution. Union wrapper for
                  ///< such case that job is non-trivial (prevents
                  ///< construction).
  };
  timer_t _preempt_timer; ///< Timer for SIGALRM preemption generation.

  const size_t _stack_size; ///< Stack size for new context frames.

  /** Kills the worker, cleaning resources if necessary, and exiting the thread
   * successfully.
   */
  auto die() -> void;

  /** Executes a new job on the worker thread.
   */
  auto run_job(Job &job) -> void;

  /** Schedules a job onto an **idle** worker thread. Awaits on the master's
   * job queue semaphore.
   */
  auto schedule() -> void;

  /** Reschedules a running worker thread upon preemption or manual yield. No
   * waiting on job queue semaphores.
   */
  auto reschedule() -> void;

  /** Cleans job resources after execution has been completed.
   */
  auto finalize_job() -> void;

  /** Static member function targetted by exit target context. Wraps finalize
   * method.
   */
  static void finalize(Worker *self) { self->finalize_job(); }

  /** Static member function acting as SIGALRM preemption signal handler. Simply
   * yields currently executing job.
   */
  static void preempt(int sig, siginfo_t *data, void *context) {
    Worker *self = (Worker *)(data->si_value.sival_ptr);
    self->yield();
  }

  /** Static member function for worker thread entry. Sets up the thread and
   * prepares worker for job execution.
   */
  static auto worker_thread_entry(Worker *self) -> void {
    self->_tid = syscall(SYS_gettid);

    // Set up SIGALRM signal handling.
    sigaction_t action;
    {
      // Handler function pointer
      action.sa_sigaction = Worker::preempt;
      // Sends extra arguments to handler (needed for self custom arg)
      action.sa_flags = SA_SIGINFO;
    }
    sigevent_t event;
    {
      // Generate signal on a specific TID.
      event.sigev_notify = SIGEV_THREAD_ID;
      // Timer signal.
      event.sigev_signo = SIGALRM;
      // Pass the worker object pointer to the handler.
      event.sigev_value.sival_ptr = self;
      // Specify the TID to generate signal on (this TID).
      // glibc bug prevents linking with gettid directly, must use syscall.
      event._sigev_un._tid = self->_tid;
    }

    // Remove any existing signal masks, register our action, prepare timer.
    sigemptyset(&action.sa_mask);
    sigaction(SIGALRM, &action, 0);
    timer_create(CLOCK_THREAD_CPUTIME_ID, &event, &self->_preempt_timer);

    // Create our exit target (hooks to static finalize member function).
    getcontext(&self->_job_exit_target);
    self->_job_exit_target.uc_stack.ss_sp = malloc(self->_stack_size);
    self->_job_exit_target.uc_stack.ss_size = self->_stack_size;
    self->_job_exit_target.uc_stack.ss_flags = 0;
    self->_job_exit_target.uc_link = 0;
    makecontext(&self->_job_exit_target, (void (*)())Worker::finalize, 1, self);

    // And now we're ready for jobs!
    self->schedule();
  }
};

} // namespace internal
} // namespace lwt
