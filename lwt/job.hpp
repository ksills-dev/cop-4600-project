#pragma once

#include <ucontext.h>

namespace lwt {

namespace internal { // Users should not touch this.

/** Convenience struct containing all the necessary bits for a job.
 * TODO: Wouldn't it be smarter to alloc the structure rather than individual
 * fields?
 */
struct Job {
  ucontext_t *context;     ///< Context for the job's actual execution.
  ucontext_t *exit_target; ///< Contains the exit target by which a worker
                           ///< thread can continue life after a job finishes.
  bool *reentrant; ///< Set to false after getcontext and set to true just
                   ///< before setcontext. Provides a means to prevent an
                   ///< infinite loop between the two.
};

} // namespace internal

} // namespace lwt