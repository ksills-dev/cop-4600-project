#include "foreman.hpp"

#include <pthread.h>
#include <semaphore.h>
#include <time.h>

namespace lwt {

namespace {
using Worker = internal::Worker; // Basic alias.
}

Foreman::Foreman(size_t worker_count, size_t preemption_interval,
                 size_t stack_size)
    : _job_queue(), _jq_lock(PTHREAD_MUTEX_INITIALIZER), _preempt_time(),
      _alive(true), _stack_size(stack_size), _n_workers(worker_count) {
  // Establish queue semaphore.
  _free_jobs = new sem_t;
  sem_init(_free_jobs, 0, -1);

  // Establish timerspec for worker preemption timers.
  _preempt_time.it_interval.tv_sec = 0;
  _preempt_time.it_interval.tv_nsec = 0;
  _preempt_time.it_value.tv_sec = 0;
  _preempt_time.it_value.tv_nsec = 1000 * preemption_interval;

  // Define number of workers and generate.
  _workers = (Worker *)calloc(_n_workers, sizeof(Worker));
  for (size_t i = 0; i < _n_workers; i++) {
    new (_workers + i) Worker(this);
  }
}

auto Foreman::worker_count() const -> size_t { return _n_workers; }

Foreman::~Foreman() {
  _alive = false; // Notify workers death is occuring (die if no more fresh jobs
                  // available).
  for (size_t i = 0; i < _n_workers; i++) {
    pthread_join(_workers[i]._thread_handle, nullptr);
    _workers[i].~Worker();
  }
  free(_workers);

  sem_destroy(_free_jobs);
  delete _free_jobs;
}

} // namespace lwt
