#pragma once

#include "future.hpp"
#include "job.hpp"
#include "worker.hpp"

#include <semaphore.h>
#include <sys/mman.h>
#include <time.h>
#include <ucontext.h>

#include <atomic>
#include <functional>
#include <queue>
#include <tuple>

#define LWT_FOREMAN_DEFAULT_STACK_SIZE 8192
#define LWT_FOREMAN_DEFAULT_WORKER_COUNT 32
#define LWT_FOREMAN_DEFAULT_INTERVAL 16000

namespace lwt {

namespace internal { // Users should not touch this.

/** Structure containing the return type, callable function type, and necessary
 * arguments for execute_callable context entrance.
 *
 * @note This must be passed as a pointer with appropriate lifetime. See
 * execute_callable for details.
 *
 * @see execute_callable
 * @see Foreman
 * @internal
 */
template <class Ret> struct Callable_Package {
  Future<Ret> future;            ///< Copy of returning future
  std::function<Ret()> callable; ///< Copy of callable function object.
};

} // namespace internal

/** Asynchronous, preempting, round-robin job pool and manager.
 *
 * @see Future
 */
class Foreman {
public:
  /** Constructor.
   * @param worker_count Designates the number of workers to spawn.
   * @param preemption_interval The time, in microseconds, for RR preemption
   * interrupts.
   * @param stack_size The size, in bytes, for new context stack frames.
   */
  Foreman(size_t worker_count = LWT_FOREMAN_DEFAULT_WORKER_COUNT,
          size_t preemption_interval = LWT_FOREMAN_DEFAULT_INTERVAL,
          size_t stack_size = LWT_FOREMAN_DEFAULT_STACK_SIZE);

  /** Destructor.
   * Will await for all jobs in queue to be completed and worker threads
   * rejoined (non-negotiable).
   */
  ~Foreman();

  // Deleting these stupid things, they make no sense.
  Foreman(Foreman const &original) = delete;
  auto operator=(Foreman const &original) -> Foreman & = delete;

  auto worker_count() const -> size_t;
  // TODO: Should be setter equivalent -> Foreman&.
  // However, this would require realloc of existing workers and, by extension,
  // invalidation of a few self pointers used. Design this extension properly
  // before a hacky implementation.

  // TODO: Some thread pool management methods go here.

  /** Execute the provided callable asynchronously with the provided arguments.
   * Returns a future by which one may access the return value.
   */
  template <class Ret, class... Args>
  auto async(std::function<Ret(Args...)> callable, Args... args)
      -> Future<Ret> {
    // This function is a nightmare.

    // As makecontext will invalidate value types passed in, create a callable
    // package on the heap with the information necessary for running the job.
    auto result = Future<Ret>();
    auto package = new internal::Callable_Package<Ret>{
        result, std::bind(callable, args...)};

    // Create the job object to store job runtime information.
    internal::Job job;
    job.context = new ucontext_t();
    job.exit_target = new ucontext_t();
    job.reentrant = new bool(false);

    // Establish the new job's context information.
    getcontext(job.context);
    job.context->uc_stack.ss_sp = malloc(_stack_size);
    // mmap allows us to allocate memory specifically designed for use as a
    // stack.
    job.context->uc_stack.ss_size = _stack_size;
    // TODO: Some flag to allow ss_size to grow with mmap?
    job.context->uc_stack.ss_flags = 0;
    job.context->uc_link = 0;

    // We now create the stack frame for the ucontext generated above.
    // execute_callable wraps all operations necessary, has a mechanism to
    // continue worker thread execution. All parameters must be pointers.
    makecontext(job.context, (void (*)())execute_callable<Ret>, 2,
                job.exit_target, package);

    // Push our job into the queue and post it's availability to any waiting
    // worker threads.
    pthread_mutex_lock(&_jq_lock);
    _job_queue.push(job);
    pthread_mutex_unlock(&_jq_lock);
    sem_post(_free_jobs);

    // Gee, I wonder what this does?
    return Future<Ret>(result);
  }

protected:
  friend internal::Worker; // Worker child needs privleges for master ref.

  std::queue<internal::Job>
      _job_queue;           ///< Queue of jobs available for scheduling.
  pthread_mutex_t _jq_lock; ///< POSIX Mutex lock for job queue access.
  sem_t *_free_jobs;        ///< Semaphore posting when concurrent job added..

  itimerspec _preempt_time; ///< Preemption time interval.
  // TODO: If we want this to be changable after construction, we need some form
  // of synch.
  std::atomic_bool _alive; ///< Method of notification for worker exit.
  // TODO: Replace the above with a proper signalling mechanism?

  const size_t _stack_size; ///< Default stack size for new jobs.

private:
  internal::Worker *_workers; ///< Array of worker threads (avoiding vector,
                              ///< requires public Worker methods.)
  size_t const _n_workers;    ///< Count of worker threads.

  /** Static member function to wrap execution of a job's callable.
   * Calls callable, places the result into the associated future, clears
   * unused memory, then jumps to the exit target (continue worker thread at
   * worker-thread defined context).
   *
   * @note This function will be entered via makecontext function. All variables
   * *must* be pointers or args will point into stack and shall be corrupted.
   * (see prior bug causing job duplication.)
   *
   * @note Does not return. After callable exits, will set future and then
   * jump to worker thread's defined exit target context (lest worker thread
   * will die).
   *
   * @see launch
   */
  template <class Ret>
  static auto execute_callable(ucontext_t *exit_target,
                               internal::Callable_Package<Ret> *package)
      -> void {
    package->future.fulfill(package->callable());

    // We own this package, it must be cleaned after execution.
    delete package;

    // Jump to worker thread defined exit target context.
    setcontext(exit_target);
  }
};

} // namespace lwt