CC=g++
CFLAGS=-pthread -lrt -Wall
TARGET=lwt-example

default: $(TARGET)

$(TARGET): worker.o foreman.o example.cpp
	@echo "[Building target]"
	$(CC) $(CFLAGS) worker.o foreman.o example.cpp -o $(TARGET)

worker.o: lwt/worker.cpp lwt/worker.hpp lwt/job.hpp
	$(CC) $(CFLAGS) -c lwt/worker.cpp

foreman.o: worker.o lwt/foreman.cpp lwt/foreman.hpp lwt/job.hpp lwt/future.hpp
	$(CC) $(CFLAGS) -c worker.o lwt/foreman.cpp

clean:
	@rm $(TARGET)
	@rm *.o