#include "lwt/lwt.hpp"

#include <chrono>
#include <fstream>
#include <iostream>
#include <vector>

auto correctness() -> void {
  std::vector<lwt::Future<int>> futures;
  lwt::Foreman thread_pool{};
  for (auto i = 0; i < 100; i++) {
    futures.push_back(thread_pool.async<int>(std::function<int()>([i]() -> int {
      auto start_time = std::chrono::high_resolution_clock::now();
      while (std::chrono::high_resolution_clock::now() - start_time <
             std::chrono::milliseconds(100)) {
      }
      return i;
    })));
  }
  std::cout << "{";
  for (auto &future : futures) {
    std::cout << future.wait_fetch() << ", ";
    ;
  }
  std::cout << "}\n";
}

auto seq_performance(int iters, int spins) -> std::chrono::nanoseconds {
  auto start = std::chrono::high_resolution_clock::now();
  for (auto i = 0; i < iters; i++) {
    for (auto j = 0; j < spins; j++) {
      asm("");
    }
  }
  return std::chrono::high_resolution_clock::now() - start;
}

auto par_performance(int iters, int spins) -> std::chrono::nanoseconds {
  lwt::Foreman thread_pool{};
  auto start = std::chrono::high_resolution_clock::now();
  for (auto i = 0; i < iters; i++) {
    thread_pool.async(std::function<int()>([spins]() {
      for (auto j = 0; j < spins; j++) {
        asm("");
      }
      return 0;
    }));
  }
  return std::chrono::high_resolution_clock::now() - start;
}

auto main(int argc, char const *const *argv) -> int {
  std::cout << "\033[1;34m[Correctness]\033[0m" << std::endl;
  correctness();
  std::cout << std::endl;

  int const iters = 1000;
  int const spins = 100000;
  std::cout << "\033[1;34m[Performance]\033[0m" << std::endl;
  std::cout << "Iters:" << iters << std::endl;
  std::cout << "Spins:" << spins << std::endl;
  std::cout << "\033[32mSequential: \033[0m"
            << seq_performance(iters, spins).count() / 1000000 << "ms 😢\n";
  std::cout << "\033[32mParallel: \033[0m"
            << par_performance(iters, spins).count() / 1000000 << "ms 😃\n";
}
