# COP 4600 Group Project - LWT
A lightweight parallel job queue implementation using POSIX pthreads and
user contexts.

The presentation is provided in both Microsoft pptx and pdf form.
The report is provided in pdf form.

## Compiling
A makefile has been provided for ease of building. Just run `make` from the
top-level directory and the `lwt-example` target should be built. Then simply
execute `./lwt-example`.

If you'd prefer to compile manually:
  `g++ -std=c++11 -o lwt-example *.cpp lwt/*.cpp  -lrt -lpthread`